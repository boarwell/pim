type input =
  | None
  | All of string
  | Specific of (string * string)
  | Error

let get_input () =
  (* ひとつめはこのプロセスが起動したときのコマンド名 *)
  (* ocaml script.ml の場合は script.ml がひとつめ *)
  let input_arr = Sys.argv in
  match Array.length input_arr with
  | 1 -> None
  | 2 -> All input_arr.(1)
  | 3 -> Specific (input_arr.(1), input_arr.(2))
  | _ -> Error

let dispatch = function
  (* 引数が指定されなかったときはすべてのprojects表示する？ *)
  | None -> print_endline "error"
  | All p ->
    (* TODO: いまはignoreしているけどステータスコードを設定したい *)
    ignore
      (Sys.command
         (* TODO: 設定ファイルの場所をオプション（環境変数とか）で取れるようにしたい *)
         (* TODO: コマンドを返す関数を作ってここはきれいにしたい *)
         (Printf.sprintf
            "jq -r '.projects[] | select(.name == \"%s\")' ./pim.config.json" p))
  | Specific (p, f) ->
    ignore
      (Sys.command
         (Printf.sprintf "jq -r '.projects[] | select(.name == \"%s\") | .%s' ./pim.config.json" p
            f))
  | Error -> print_endline "error"

let () = get_input () |> dispatch
