# PIM

Project Information Manager

## Run

```sh
$ pim abc(project code)
{
  "name": "abc",
  "code": "abc",
  "basic": {
    "user": "hoge",
    "pass": "fuga"
  }
}

$ pim abc(project code) basic(field)
{
  "user": "hoge",
  "pass": "fuga"
}

$ pim abc(project code) basic.pass(field of field)
fuga
```
